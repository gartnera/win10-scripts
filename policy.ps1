$MachineDir = "$env:windir\system32\GroupPolicy\Machine\registry.pol"

$entries = @(
    # onedrive
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\OneDrive"
        ValueName = "DisableFileSyncNGSC"
        Data = "1"
        Type = "DWord"
    },
    # advertising info
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\AdvertisingInfo"
        ValueName = "DisabledByGroupPolicy"
        Data = "1"
        Type = "DWord"
    },
    # app compatibility
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\AppCompat"
        ValueName = "AITEnable"
        Data = "0"
        Type = "DWord"
    },
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\AppCompat"
        ValueName = "DisableInventory"
        Data = "1"
        Type = "DWord"
    },
    # customer improvement
    @{
        Key = "SOFTWARE\Policies\Microsoft\Internet Explorer\SQM"
        ValueName = "DisableCustomerImprovementProgram"
        Data = "1"
        Type = "DWord"
    },
    @{
        Key = "SOFTWARE\Policies\Microsoft\MRT"
        ValueName = "DontReportInfectionInformation"
        Data = "1"
        Type = "DWord"
    },
    @{
        Key = "SOFTWARE\Policies\Microsoft\SQMClient\Windows"
        ValueName = "CEIPEnable"
        Data = "0"
        Type = "DWord"
    },

    # smart screen
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\System"
        ValueName = "EnableSmartScreen"
        Data = "0"
        Type = "DWord"
    },
    # telemetry
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\DataCollection"
        ValueName = "AllowTelemetry"
        Data = "0"
        Type = "DWord"
    },
    # error reporting
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\Windows Error Reporting"
        ValueName = "Disabled"
        Data = "1"
        Type = "DWord"
    },
    # search
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\Windows Search"
        ValueName = "ConnectedSearchPrivacy"
        Data = "3"
        Type = "DWord"
    },
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\Windows Search"
        ValueName = "ConnectedSearchPrivacy"
        Data = "0"
        Type = "DWord"
    },
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\Windows Search"
        ValueName = "ConnectedSearchUseWeb"
        Data = "0"
        Type = "DWord"
    },
    @{
        Key = "SOFTWARE\Policies\Microsoft\Windows\Windows Search"
        ValueName = "DisableWebSearch"
        Data = "1"
        Type = "DWord"
    }
)

foreach ($entry in $entries) {
    Set-PolicyFileEntry -Path $MachineDir @entry
}